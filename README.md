# Rest_service

## LINK AL REPOSITORIO

https://gitlab.com/sad28/rest_service  

y al resto de repositorios  

https://gitlab.com/sad28

## Authors and acknowledgment  

- Adrian Muñoz Felder   
- Maria Ortiz Riera 

## Support

email at admuofel@inf.upv.es 
or at mortrie@etsinf.upv.es 


## Explication 

Para esta actividad 3 hemos echo uso de los códigos de ejemplo server_registery y service.

Tenemos un total de 4 repositorios cada uno corresponde a uno de los componentes del sistema distribuido.

REST_SERVICE

Rest_Service actua cómo Service Resgistery de los distintos servicios del sistema, el código es básicamente el mismo que el proporcionado con algún pequeño cambio, tambíen se encuentra aquí la explicación de todo el sistema, en este mismo fichero Readme y no se encunetra información en del resto.

La clase ServiceRegistry contiene un constructor donde se inicializan los parámetros a utilizar, donde el timeout que se ha indicado será de 10 segundos entre cada mensaje.

En esta clase hay declaradas 4 funciones:

· GET -> Donde la variable candidates recoge el valor del servicio que se ha realizado, si es de desarrollo, de producción o testeo (debug, info, fatal)

· REGISTER -> Primeramente se borra la sesión anterior mediante la llamada a la función cleanup(), después de esto se añade el servicio con los datos que se recogen (IP, puerto, nombre y version), todo esto es almacenado en la variable key. En caso de que ya exista el servicio simplemente se actualizarán los datos que se hayan modificado.

· UNREGISTER -> Lo que se hace es eliminar un servicio mediante los datos almacenados en la variable key. Esta función se realiza mediante una llamada desde la propia API.

· CLEANUP -> Al igual que UNREGISTER, elimina un servicio con la diferencia de que funciona como un método interno al que se llama para realizar de nuevo un GET o REGISTER.

El módulo service realiza llamadas a las funciones anteriormente descritas, exceptuando cleanup ya que como se ha comentado se llama en la propia clase, mediante request-response se consigue añadir (PUT), eliminar (DELETE o UNREGISTER) y mostrar (GET) un servicio.

CART_SERVICE

A partir de service hemos desarrollado cart-service que implementa la funcionalidad de un carrito de la compra, el código se puede encontrar en la carpeta entities en el fichero cart.js que implementa una clase con los métodos añadir eliminar y toString.  
La forma de proceder para añadir un producto es tras recibir una petición y parsear el producto que se indica en la url haciendo uso de req.params se utiliza la descripción del producto para llamar al método.

En la clase service se han definido los endpoints de la api rest de cart con tres endpoints uno GET que en la propia ruta del servicio invoca al método toString, otro PUT sobre la ruta /add e indicando el producto a añdir y por último otro PUT pero sobre la ruta /del que eliminará productos del carrito.

Este módulo permite que se puedan añadir elementos al carrito mediante el método PUT, donde se recogerán todos los datos del producto a introducir y se añadirá al carrito. Para ello, también se deberá devolver el estado 200 de la página, es decir, debe haber confirmación por parte del servidor que ha recibido correctamente la información que se ha añadido.

Para que sea visible si los elementos se añaden o se eliminan del carrito, será necesario que se muestre el carrito. Esto se puede realizar con el método GET, que se encargará de pasar todos los datos que haya en el carrito a strings, es decir, se utilizará la función toString().

En caso de que no haya respuesta por parte del servidor aparecerá el mensaje de error para conectarse a él, con el código de respuesta 500.

STOCK_SERVICE

stockController

El objetivo de este módulo es conectarse a una base de datos Mongo y comprobar el stock del producto que se quiera añadir al carrito. Si el producto tiene stock se añadirá sin problema al carrito, en caso de que no haya stock, se lanzará el error de que no hay stock de cierto producto y por tanto no se añadirá al carrito.

service.js

En el método GET se consigue comprobar si el stock es mayor que 0 o no debido a que se captura con la directiva catch el error de stock 0 lanzado anteirormenete. Se responde en el mensaje con el producto en cuestión que se ha añadido al carrito y en caso de que el stock sea 0, el mensaje que se responderá es False, es decir, dicho producto no tiene stock. En ambos casos, tenga stock o no, se recibirá el código de respuesta 200 indicando de esta manera que el servidor responde a las peticiones.   
También se ha tenido en cuenta que si en algún momento hay algún error interno en el servidor se mostrará el código de respuesta 500.

USER_SERVICE

Este último módulo es para poder mostrar el funcionamiento del sistema sin necesidad de realizar peticiones a las API's desde un cliente externo cómo Postman, que sí hemos utilizado durante el desarrollo. El servicio como el resto primeramente se registrará en el server registery y luego preguntará dónde puede encontrar el cart-service medinate un get a la ruta find del server_registery. Cuándo este le responde hacemos ahora un apetición para añadir un producto y luego un get del contenido del carrito, vemos que tiene un item "naranjas" acto seguido hacemos otraètición para eliminar el producto del carriro ahora de nuevo con get para mostrar el contenido observamos que no hay ningún contenido y se ha eliminado correctamente. 


## Useful Links

https://stackoverflow.com/questions/33509816/what-exactly-does-usr-bin-env-node-do-at-the-beginning-of-node-files

https://stackoverflow.com/questions/59470406/how-to-solve-could-not-find-any-python-installation-to-use-with-docker-node-al